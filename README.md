# AgroDataScience

Science des données pour l’agronomie et l’agroécologie : notes de cours

Livre bookdown de Data Science appliquée à l'agronomie sens large.

http://benedicte.fontez.pages.mia.inra.fr/agrodatascience/

Licence GPL-3 - l'institut Agro - Montpellier SupAgro - 2021
UMR MISTEA & UMR ABSys

* Meïli Baragatti [aut]
* Bénédicte Fontez [cre, aut]
* Léo Garcia [aut]
* Isabelle Sanchez [ctr]

Livré créé avec le package **bookdown** dans R: https://bookdown.org/yihui/bookdown/ et compilé dans la forgemia Gitlab: https://about.gitlab.com/ 

IS 05/10/2021:
* correction de syntaxe dans 00: bcp de problèmes latex principalement, faire attention aux équations, ne pas utliser les mots clés `eqnarray*` (i.e. avec le suffixe étoile), bien encadrer les mots avec {} etc... 
* suppression de l'appel de **qpcR** dans 02-: l'image docker n'arrive pas à l'installer... j'ai mis les chunk en eval=FALSE afin que le code aparaisse mais qu'il ne soit pas executé => je continue de chercher à corriger ce bug docker mais vous devriez chercher de votre côté des fonctions PRESS et RMSE disponibles dans d'autres packages (où écrire les fonctions vous-mêmes...)
* rajout de la figure SchemaTest.png
* suppression d'une reference à une table kable qui ne fonctionnait pas
* il faudra revoir le formatage des tableaux d'ANOVA car ils sont trop longs dans le pdf (même si très bien dans le HTML)

Je vous propose:
1/ de vous créer une branche par auteur, ainsi vous ne travaillez que sur votre branche et ne faites des modifications qu'à cet endroit. cela n'impacte pas le programme principal (la branche `main`)
2/ lorsque vous souhaitez incorporer un nouveau fichier ou un fichier modifié, vous faites une demande: une `request`. Ainsi je reçois la demande, je peux voir le fichier avant de l'incorporer et ne l'intègre dans `main` que s'il est OK.
3/ merci de faire des request que **fichier par fichier**. C'est ce qui a été aussi long dans le précédent debbugage: quasi tous les fichiers avaient été beaucoup modifiés et les erreurs étaient difficiles à discerner les unes des autres. (le pb d'install de **qpcR** m'a pris 2 jours).
4/ d'un point de vue éditorial, essayez d'homogénéiser le style (français et coding). Là, les styles des chapitres sont très différents les uns de autres
