EXPLICATION JEUX DE DONNEES

Mod�les lin�aires simples et mod�les lin�aires mixtes.

- pois_chiche_VAT.xls : jeu de donn�es qui permet de faire des anovas
  ANOVA sur  les variables rendements  ou biomasse 3m2 ou hauteur, ou Floraison...avec le facteur Var et la r�p�tition (qui est en fait le bloc) comme variables explicatives
  REG LIN simple :  par exemple IR (indice de r�colte) en fonction de Biomasse 3m2
  ANCOVA idem, avec Var comme facteur explicatif. On peut aussi regarder le srelationsnetre Biomasse et hauteur
  MODELE MIXTE :  VAR peut eter consid�r� comme un effet al�atoire --> on peut �tudier les m�mes mod�les que ci-dessus mais aevc VAR en effet al�atoire
  
  -haricots.txt :  ANOVA (sur DM Spar exemple) � 2 facteurs (Vari�t� et Nitrogen) et 1 bloc
  -laitues.txtx :  ANOVA (sur MS) � un facteur (varit�t�) et un bloc (parcelle)
  -�valuation modele.txt :  regression lin�aire sur ATSWsim en fonction de ATSWmeas ou sur BMsim en fonction de BMmeas. 
  		   On peut ajouter le facteur "Treat" ou bien "Water" et "Nitrogen" pour faire uen ANCOVA
  -ITK valensole.txt: jeu secours pas tr�s itersseant : on peut �tudieer MSN (marge semi nette) en fonction de Rdt (rendement) avec 
  		   Agriculteur ou Parcelle ou IFT (Indice de Fr�quance de Traitement) 
  		   
  		   
 Mod�les g�n�ralis� - loi de poisson
 -weeds.csv :  �tude sur l'abondance (ab_tot) et la diversit� d'adventices (rich_spe) en agroforesterie en fonction de la ditsnace � l'arbre (LSA)
 		modele de regression entre ab_to LSA ou entre rich_sp et LSA. le facteur systeme peut eter ajout� en ANCOVA
 		on peut aussi ajouter transect et parcelles comme facteurs al�atoires!
 -aphids :  infestation de pucerons (scoring) sur diverse accessions= vari�t� (acno) de sojas, sur plusieurs tests. On peut travailler sur un seul test ou le passer en facteur (al�atoire!)
            ANOVA sur scoring avec acno comme facteur.
            
       
 Mod�les d eregression non lin�aire (croissance logistique)
 -GPAI_reg_non_lin.csv (GPAI en fonction de DAP. On peut tester le facteur densit�)
 -vignes.csv :  gs.rel en fonction de FTSW. on peut ajouter ann�e ou traitement comme facteur
 -greenhouse.csv
 -regNonLinFINT_pois_chiche.csv :  f en fonction de DAS ou TT. Geno peut eter test� comme facteur
 
