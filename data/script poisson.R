# Reg non linéaire

# Packages utilisés pendant le cours
library(ggplot2)
library(sandwich)
library(msm)
library(dplyr)

# importation des données
setwd("~/PVD/Cours/data")
don <- read.csv("weeds_simp.csv", sep=";")

summary(don[,c(5,7,8)])

# mise en forme des données
library(dplyr)
don$session <- factor(don$session,levels=1:2,labels=c("s1","s2"))
don %>% group_by(systeme) %>% summarise(avg=mean(rich_sp,na.rm=TRUE), 
                                        variance = var(rich_sp,na.rm=TRUE))
