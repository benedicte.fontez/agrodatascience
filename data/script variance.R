# GLM - variance hétérogène

setwd("~/PVD/Cours/data")
vignes <- read.csv2("vignes.csv")
head(vignes)

#regression linéaire simple
mod1 <- lm(gs.rel~FTSW, data=vignes)
summary(mod1)
plot(residuals(mod1)~predict(mod1))
plot(residuals(mod1)~FTSW,data=vignes)
abline(h = 0,col="red")
plot(mod1, which=c(3,4))

# régression linéaire pondérée
mod2 <- lm(gs.rel~FTSW,weights = 1/FTSW, data=vignes)
summary(mod2)
plot(mod2,which=3)
plot(rstandard(mod2)~predict(mod2))

# limite modèle avec fonction lm() weights non connus/calculables
mod3 <- lm(gs.rel~FTSW,weights = 1/predict(mod1), data=vignes)
summary(mod3)
plot(mod3,which=3)

AIC(mod2,mod3)
BIC(mod2,mod3)

#---------------------gls
library(nlme)
res.gls <- gls(gs.rel~FTSW,weights=~FTSW,data=vignes)

summary(res.gls)

res.gls2<-gls(gs.rel~FTSW, weights=varPower(),data=vignes)
summary(res.gls2)

